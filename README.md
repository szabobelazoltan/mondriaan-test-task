# Read me

## Introduction
This application is created for Mondriaan's candidate test.

## Versions
Java language version: 8
Maven build tool version: 3.6.3

## Running application
To run this application use the following Maven command:

    mvn clean compile exec:java