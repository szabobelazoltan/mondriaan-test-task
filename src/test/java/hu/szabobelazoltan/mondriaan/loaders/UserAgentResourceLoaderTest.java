package hu.szabobelazoltan.mondriaan.loaders;

import hu.szabobelazoltan.mondriaan.loader.UserAgentResourceLoader;
import hu.szabobelazoltan.mondriaan.loader.UserAgentResourceLoaderImpl;
import hu.szabobelazoltan.mondriaan.parsers.ParserProvider;
import hu.szabobelazoltan.mondriaan.parsers.UserAgentParser;
import hu.szabobelazoltan.mondriaan.vos.UserAgentVO;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(JUnit4.class)
public class UserAgentResourceLoaderTest {

    private UserAgentResourceLoader loader;

    @Before
    public void setUp() {
        UserAgentParser<UserAgentVO> parser = ParserProvider.USER_AGENT_PARSER;
        loader = new UserAgentResourceLoaderImpl(parser);
    }

    @Test
    public void testLoad() {
        loader.load();
        List<UserAgentVO> result = loader.getUserAgents();

        assertNotNull(result);
        assertEquals(4096, result.size());
    }
}
