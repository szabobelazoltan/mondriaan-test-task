package hu.szabobelazoltan.mondriaan.loaders;

import hu.szabobelazoltan.mondriaan.loader.EntityAdapter;
import hu.szabobelazoltan.mondriaan.loader.EntityAdapterImpl;
import hu.szabobelazoltan.mondriaan.model.Application;
import hu.szabobelazoltan.mondriaan.model.Device;
import hu.szabobelazoltan.mondriaan.model.Platform;
import hu.szabobelazoltan.mondriaan.model.UserAgent;
import hu.szabobelazoltan.mondriaan.vos.ApplicationVO;
import hu.szabobelazoltan.mondriaan.vos.DeviceVO;
import hu.szabobelazoltan.mondriaan.vos.PlatformVO;
import hu.szabobelazoltan.mondriaan.vos.UserAgentVO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class EntityAdapterTest {
    @Test
    public void testWithCompleteVo() {
        DeviceVO deviceVO = new DeviceVO("SAMSUNG", "SM-A520F");
        PlatformVO platformVO = new PlatformVO("Android", "8.0.0");
        ApplicationVO applicationVO1 = new ApplicationVO("Chrome", "71.0.3578.99");
        ApplicationVO applicationVO2 = new ApplicationVO("Evening+News", "5.10.6");

        UserAgentVO userAgentVO1 = new UserAgentVO(deviceVO, platformVO, applicationVO1);
        UserAgentVO userAgentVO2 = new UserAgentVO(deviceVO, platformVO, applicationVO2);
        List<UserAgentVO> userAgentVOs = Arrays.asList(userAgentVO1, userAgentVO2);

        EntityAdapter entityAdapter = new EntityAdapterImpl();
        entityAdapter.processVOs(userAgentVOs);

        List<UserAgent> userAgentEntities = entityAdapter.getUserAgentEntities();
        assertNotNull(userAgentEntities);
        assertEquals(userAgentVOs.size(), userAgentEntities.size());

        UserAgent userAgentEntity = userAgentEntities.get(0);
        assertNotNull(userAgentEntity);
        assertNotNull(userAgentEntity.getApplication());
        assertNotNull(userAgentEntity.getPlatform());
        assertNotNull(userAgentEntity.getDevice());

        userAgentEntity = userAgentEntities.get(1);
        assertNotNull(userAgentEntity);
        assertNotNull(userAgentEntity.getApplication());
        assertNotNull(userAgentEntity.getPlatform());
        assertNotNull(userAgentEntity.getDevice());

        List<Device> deviceEntities = entityAdapter.getDeviceEntities();
        assertNotNull(deviceEntities);
        assertEquals(1, deviceEntities.size());

        Device deviceEntity = deviceEntities.get(0);
        assertNotNull(deviceEntity);
        assertEquals(deviceVO.getVendor(), deviceEntity.getVendor());
        assertEquals(deviceVO.getModel(), deviceEntity.getModel());

        List<Platform> platformEntities = entityAdapter.getPlatformEntities();
        assertNotNull(platformEntities);
        assertEquals(1, platformEntities.size());

        Platform platformEntity = platformEntities.get(0);
        assertNotNull(deviceEntity);
        assertEquals(platformVO.getFamily(), platformEntity.getFamily());
        String[] version = platformVO.getVersion().split("\\.|_");
        assertEquals(Integer.parseInt(version[0]), platformEntity.getMajorVersion().intValue());
        assertEquals(version.length > 1 ? Integer.parseInt(version[1]) : null, platformEntity.getMinorVersion());
        assertEquals(version.length > 2 ? Integer.parseInt(version[2]) : null, platformEntity.getPatchVersion());

        List<Application> appEntities = entityAdapter.getApplicationEntities();
        assertNotNull(appEntities);
        assertEquals(2, appEntities.size());

        Application appEntity = appEntities.get(0);
        assertNotNull(appEntity);
        assertEquals(applicationVO1.getName(), appEntity.getName());
        assertEquals(applicationVO1.getVersion(), appEntity.getVersion());

        appEntity = appEntities.get(1);
        assertNotNull(appEntity);
        assertEquals(applicationVO2.getName(), appEntity.getName());
        assertEquals(applicationVO2.getVersion(), appEntity.getVersion());
    }

    @Test
    public void testWithMissingDeviceVo() {
        PlatformVO platformVO = new PlatformVO("Android", "8.0.0");
        ApplicationVO applicationVO = new ApplicationVO("Chrome", "71.0.3578.99");
        UserAgentVO userAgentVO = new UserAgentVO(null, platformVO, applicationVO);

        EntityAdapter entityAdapter = new EntityAdapterImpl();
        entityAdapter.processVOs(Collections.singletonList(userAgentVO));

        List<UserAgent> userAgentEntities = entityAdapter.getUserAgentEntities();
        assertNotNull(userAgentEntities);
        assertEquals(1, userAgentEntities.size());

        UserAgent userAgentEntity = userAgentEntities.get(0);
        assertNotNull(userAgentEntity);
        assertNotNull(userAgentEntity.getApplication());
        assertNotNull(userAgentEntity.getPlatform());
        assertNull(userAgentEntity.getDevice());

        List<Device> deviceEntities = entityAdapter.getDeviceEntities();
        assertNotNull(deviceEntities);
        assertTrue(deviceEntities.isEmpty());
    }
}
