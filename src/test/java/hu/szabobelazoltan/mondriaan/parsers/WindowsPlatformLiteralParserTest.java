package hu.szabobelazoltan.mondriaan.parsers;

import hu.szabobelazoltan.mondriaan.vos.PlatformLiteralVO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class WindowsPlatformLiteralParserTest {
    private UserAgentParser<PlatformLiteralVO> parser = new WindowsPlatformLiteralParser();

    @Test
    public void testWithValidPlatformInfo() {
        String platformInfo = "Windows NT 6.3; Win64; x64";

        PlatformLiteralVO result = parser.parse(platformInfo);

        assertNotNull(result);
        assertEquals("Windows NT", result.getPlatformFamily());
        assertEquals("6.3", result.getPlatformVersionNumber());
        assertNull(result.getDeviceVendor());
        assertNull(result.getDeviceModel());
    }

    @Test
    public void testWithInvalidPlatformInfo() {
        String platformInfo = "Linux; Android 8.0.0; SAMSUNG SM-A520F";

        PlatformLiteralVO result = parser.parse(platformInfo);

        assertNull(result);
    }
}
