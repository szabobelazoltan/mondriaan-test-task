package hu.szabobelazoltan.mondriaan.parsers;

import hu.szabobelazoltan.mondriaan.vos.UserAgentLiteralVO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class AmazonUserAgentLiteralParserTest {
    private final UserAgentParser<UserAgentLiteralVO> parser = new AmazonUserAgentLiteralParser();

    @Test
    public void testWithValidUserAgent() {
        String userAgent = "Amazon CloudFront";

        UserAgentLiteralVO result = parser.parse(userAgent);

        assertNotNull(result);
        assertEquals("Amazon CloudFront", result.getApplicationName());
        assertNull(result.getApplicationVersionNumber());
        assertNull(result.getPlatformInfo());
    }

    @Test
    public void testWithInvalidUserAgent() {
        String userAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36";

        UserAgentLiteralVO result = parser.parse(userAgent);

        assertNull(result);
    }
}
