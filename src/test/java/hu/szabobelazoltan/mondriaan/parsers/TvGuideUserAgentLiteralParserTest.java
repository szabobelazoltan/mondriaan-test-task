package hu.szabobelazoltan.mondriaan.parsers;

import hu.szabobelazoltan.mondriaan.vos.UserAgentLiteralVO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class TvGuideUserAgentLiteralParserTest {
    private UserAgentParser<UserAgentLiteralVO> parser = new TvGuideUserAgentLiteralParser();

    @Test
    public void testWithValidLiteral() {
        String userAgent = "TV-Guide/4501 CFNetwork/758.5.3 Darwin/15.6.0";

        UserAgentLiteralVO result = parser.parse(userAgent);

        assertNotNull(result);
        assertEquals("TV-Guide", result.getApplicationName());
        assertEquals("4501", result.getApplicationVersionNumber());
        assertNull(result.getPlatformInfo());
    }

    @Test
    public void testWithInvalidLiteral() {
        String userAgent = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.100 Safari/537.36";

        UserAgentLiteralVO result = parser.parse(userAgent);

        assertNull(result);
    }
}
