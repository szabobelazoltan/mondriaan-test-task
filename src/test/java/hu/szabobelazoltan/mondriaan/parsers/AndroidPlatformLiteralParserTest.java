package hu.szabobelazoltan.mondriaan.parsers;

import hu.szabobelazoltan.mondriaan.vos.PlatformLiteralVO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class AndroidPlatformLiteralParserTest {
    private UserAgentParser<PlatformLiteralVO> parser = new AndroidPlatformLiteralParser();

    @Test
    public void testWithValidPlatformLiteralContainingDeviceVendor1() {
        String platformLiteral = "Linux; Android 8.0.0; SAMSUNG SM-A520F";

        PlatformLiteralVO result = parser.parse(platformLiteral);

        assertNotNull(result);
        assertEquals("Android", result.getPlatformFamily());
        assertEquals("8.0.0", result.getPlatformVersionNumber());
        assertEquals("SAMSUNG", result.getDeviceVendor());
        assertEquals("SM-A520F", result.getDeviceModel());
    }

    @Test
    public void testWithValidPlatformLiteralContainingDeviceVendor2() {
        String platformLiteral = "Linux; U; Android 5.1.1; HUAWEI M2-A01L Build/HUAWEIM2-A01L";

        PlatformLiteralVO result = parser.parse(platformLiteral);

        assertNotNull(result);
        assertEquals("Android", result.getPlatformFamily());
        assertEquals("5.1.1", result.getPlatformVersionNumber());
        assertEquals("HUAWEI", result.getDeviceVendor());
        assertEquals("M2-A01L", result.getDeviceModel());
    }

    @Test
    public void testWithValidPlatformLiteralMissingDeviceVendor1() {
        String platformLiteral = "Linux; U; Android 9; SM-A505FN Build/PPR1.180610.011";

        PlatformLiteralVO result = parser.parse(platformLiteral);

        assertNotNull(result);
        assertEquals("Android", result.getPlatformFamily());
        assertEquals("9", result.getPlatformVersionNumber());
        assertNull(result.getDeviceVendor());
        assertEquals("SM-A505FN", result.getDeviceModel());
    }
    @Test
    public void testWithValidPlatformLiteralMissingDeviceVendor2() {
        String platformLiteral = "Linux; U; Android 8.0.0; G3112 Build/48.1.A.2.101";

        PlatformLiteralVO result = parser.parse(platformLiteral);

        assertNotNull(result);
        assertEquals("Android", result.getPlatformFamily());
        assertEquals("8.0.0", result.getPlatformVersionNumber());
        assertNull(result.getDeviceVendor());
        assertEquals("G3112", result.getDeviceModel());
    }
}
