package hu.szabobelazoltan.mondriaan.parsers;

import hu.szabobelazoltan.mondriaan.vos.PlatformLiteralVO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class MacintoshPlatformLiteralParserTest {
    private UserAgentParser<PlatformLiteralVO> parser = new MacintoshPlatformLiteralParser();

    @Test
    public void testWithValidPlatformLiteral() {
        String platform = "Macintosh; Intel Mac OS X 10_14_6";

        PlatformLiteralVO result = parser.parse(platform);

        assertNotNull(result);
        assertEquals("Mac OS X", result.getPlatformFamily());
        assertEquals("10_14_6", result.getPlatformVersionNumber());
        assertNull(result.getDeviceVendor());
        assertEquals("Macintosh", result.getDeviceModel());
    }

    @Test
    public void testWithInvalidPlatformLiteral() {
        String platform = "Linux; U; Android 8.0.0; SM-A320FL Build/R16NW";

        PlatformLiteralVO result = parser.parse(platform);

        assertNull(result);
    }
}
