package hu.szabobelazoltan.mondriaan.parsers;

import hu.szabobelazoltan.mondriaan.vos.UserAgentLiteralVO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class ElbHealthCheckerUserAgentLiteralParserTest {
    private UserAgentParser<UserAgentLiteralVO> parser = new ElbHealthCheckerUserAgentLiteralParser();

    @Test
    public void testWithValidLiteral() {
        String literal = "ELB-HealthChecker/2.0";

        UserAgentLiteralVO result = parser.parse(literal);

        assertNotNull(result);
        assertEquals("ELB-HealthChecker", result.getApplicationName());
        assertEquals("2.0", result.getApplicationVersionNumber());
        assertNull(result.getPlatformInfo());
    }

    @Test
    public void testWithInvalidLiteral() {
        String literal = "Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.106 Safari/537.36";

        UserAgentLiteralVO result = parser.parse(literal);

        assertNull(result);
    }
}
