package hu.szabobelazoltan.mondriaan.parsers;

import hu.szabobelazoltan.mondriaan.vos.PlatformLiteralVO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class ApplePlatformLiteralParserTest {
    private UserAgentParser<PlatformLiteralVO> parser = new ApplePlatformLiteralParser();

    @Test
    public void testWithIphoneLiteral() {
        String platformLiteral = "iOS/13.3.1 (iPhone11,8)";

        PlatformLiteralVO result = parser.parse(platformLiteral);

        assertNotNull(result);
        assertEquals("iOS", result.getPlatformFamily());
        assertEquals("13.3.1", result.getPlatformVersionNumber());
        assertEquals("iPhone11,8", result.getDeviceModel());
        assertNull(result.getDeviceVendor());
    }

    @Test
    public void testWithIpadLiteral() {
        String platformLiteral = "iOS/12.0 (iPad5,3)";

        PlatformLiteralVO result = parser.parse(platformLiteral);

        assertNotNull(result);
        assertEquals("iOS", result.getPlatformFamily());
        assertEquals("12.0", result.getPlatformVersionNumber());
        assertEquals("iPad5,3", result.getDeviceModel());
        assertNull(result.getDeviceVendor());
    }

    @Test
    public void testWithInvalidLiteral() {
        String platformLiteral = "Linux; U; Android 7.1.1; SM-J510FN Build/NMF26X";

        PlatformLiteralVO result = parser.parse(platformLiteral);

        assertNull(result);
    }
}
