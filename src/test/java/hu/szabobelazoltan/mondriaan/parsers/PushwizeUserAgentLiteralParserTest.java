package hu.szabobelazoltan.mondriaan.parsers;

import hu.szabobelazoltan.mondriaan.vos.UserAgentLiteralVO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(JUnit4.class)
public class PushwizeUserAgentLiteralParserTest {
    private UserAgentParser<UserAgentLiteralVO> parser = new PushwizeUserAgentLiteralParser();

    @Test
    public void testWithValidLiteral1() {
        String uaLiteral = "Pushwize/2.8.0 Dutch+News/9.14.0 (com.mondriaan.dutchnews/113530) Dalvik/2.1.0 (Linux; U; Android 9; POT-LX1 Build/HUAWEIPOT-L21)";

        UserAgentLiteralVO result = parser.parse(uaLiteral);

        assertNotNull(result);
        assertEquals("Dutch+News", result.getApplicationName());
        assertEquals("9.14.0", result.getApplicationVersionNumber());
        assertEquals("Linux; U; Android 9; POT-LX1 Build/HUAWEIPOT-L21", result.getPlatformInfo());
    }

    @Test
    public void testWithValidLiteral2() {
        String uaLiteral = "Pushwize/1.9.3 Radio/46 (com.mondriaan.radio.finnish/740) iOS/13.2 (iPhone8,4)";

        UserAgentLiteralVO result = parser.parse(uaLiteral);

        assertNotNull(result);
        assertEquals("Radio", result.getApplicationName());
        assertEquals("46", result.getApplicationVersionNumber());
        assertEquals("iOS/13.2 (iPhone8,4)", result.getPlatformInfo());
    }
}
