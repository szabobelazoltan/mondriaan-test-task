package hu.szabobelazoltan.mondriaan.parsers;

import hu.szabobelazoltan.mondriaan.vos.UserAgentLiteralVO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class ChromeUserAgentParserTest {
    private UserAgentParser<UserAgentLiteralVO> parser = new ChromeUserAgentParser();

    @Test
    public void testWithValidWindowsHostedUserAgentLiteral() {
        String uaLiteral = "Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.106 Safari/537.36";

        UserAgentLiteralVO result = parser.parse(uaLiteral);

        assertNotNull(result);
        assertEquals("Chrome", result.getApplicationName());
        assertEquals("80.0.3987.106", result.getApplicationVersionNumber());
        assertEquals("Windows NT 6.3; Win64; x64", result.getPlatformInfo());
    }

    @Test
    public void testWithValidAndroidHostedUserAgentLiteral() {
        String uaLiteral = "Mozilla/5.0 (Linux; Android 8.0.0; SAMSUNG SM-A520F) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/10.2 Chrome/71.0.3578.99 Mobile Safari/537.36";

        UserAgentLiteralVO result = parser.parse(uaLiteral);

        assertNotNull(result);
        assertEquals("Chrome", result.getApplicationName());
        assertEquals("71.0.3578.99", result.getApplicationVersionNumber());
        assertEquals("Linux; Android 8.0.0; SAMSUNG SM-A520F", result.getPlatformInfo());
    }

    @Test
    public void testWithInvalidLiteral() {
        String uaLiteral = "ELB-HealthChecker/2.0";

        UserAgentLiteralVO result = parser.parse(uaLiteral);

        assertNull(result);
    }
}
