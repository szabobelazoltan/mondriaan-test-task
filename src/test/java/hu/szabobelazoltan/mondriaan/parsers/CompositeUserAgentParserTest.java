package hu.szabobelazoltan.mondriaan.parsers;

import hu.szabobelazoltan.mondriaan.vos.PlatformLiteralVO;
import hu.szabobelazoltan.mondriaan.vos.UserAgentLiteralVO;
import hu.szabobelazoltan.mondriaan.vos.UserAgentVO;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class CompositeUserAgentParserTest {
    @Mock
    private UserAgentParser<UserAgentLiteralVO> userAgentLiteralParser;
    @Mock
    private UserAgentParser<PlatformLiteralVO> platformLiteralParser;

    private UserAgentParser<UserAgentVO> parserToTest;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.parserToTest = new CompositeUserAgentParser(this.userAgentLiteralParser, this.platformLiteralParser);
    }

    @Test
    public void testWithCompleteUserAgentLiteral() {
        String userAgentString = "Mozilla/5.0 (Linux; Android 8.0.0; SAMSUNG SM-A520F) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/10.2 Chrome/71.0.3578.99 Mobile Safari/537.36";
        String platformInfoString = "Linux; Android 8.0.0; SAMSUNG SM-A520F";

        UserAgentLiteralVO userAgentLiteral = new UserAgentLiteralVO("Chrome", "71.0.3578.99", platformInfoString);
        when(this.userAgentLiteralParser.parse(eq(userAgentString))).thenReturn(userAgentLiteral);

        PlatformLiteralVO platformLiteral = new PlatformLiteralVO("Android", "8.0.0", "SAMSUNG", "SM-A520F");
        when(this.platformLiteralParser.parse(eq(platformInfoString))).thenReturn(platformLiteral);

        UserAgentVO result = this.parserToTest.parse(userAgentString);

        assertNotNull(result);
        assertNotNull(result.getDevice());
        assertEquals(platformLiteral.getDeviceVendor(), result.getDevice().getVendor());
        assertEquals(platformLiteral.getDeviceModel(), result.getDevice().getModel());
        assertNotNull(result.getPlatform());
        assertEquals(platformLiteral.getPlatformFamily(), result.getPlatform().getFamily());
        assertEquals(platformLiteral.getPlatformVersionNumber(), result.getPlatform().getVersion());
        assertNotNull(result.getApplication());
        assertEquals(userAgentLiteral.getApplicationName(), result.getApplication().getName());
        assertEquals(userAgentLiteral.getApplicationVersionNumber(), result.getApplication().getVersion());
    }

    @Test
    public void testWithMissingDeviceInfoUserAgentLiteral() {
        String userAgentString = "Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.106 Safari/537.36";
        String platformInfoString = "Windows NT 6.3; Win64; x64";

        UserAgentLiteralVO userAgentLiteral = new UserAgentLiteralVO("Chrome", "80.0.3987.106", platformInfoString);
        when(this.userAgentLiteralParser.parse(eq(userAgentString))).thenReturn(userAgentLiteral);

        PlatformLiteralVO platformLiteral = new PlatformLiteralVO("Windows NT", "6.3", null, null);
        when(this.platformLiteralParser.parse(eq(platformInfoString))).thenReturn(platformLiteral);

        UserAgentVO result = this.parserToTest.parse(userAgentString);

        assertNotNull(result);
        assertNull(result.getDevice());
        assertNotNull(result.getPlatform());
        assertEquals(platformLiteral.getPlatformFamily(), result.getPlatform().getFamily());
        assertEquals(platformLiteral.getPlatformVersionNumber(), result.getPlatform().getVersion());
        assertNotNull(result.getApplication());
        assertEquals(userAgentLiteral.getApplicationName(), result.getApplication().getName());
        assertEquals(userAgentLiteral.getApplicationVersionNumber(), result.getApplication().getVersion());
    }

    @Test
    public void testWithMissingPlatformLiteralUserAgentLiteral() {
        String userAgentString = "ELB-HealthChecker/2.0";

        UserAgentLiteralVO userAgentLiteral = new UserAgentLiteralVO("ELB-HealthChecker", "2.0", null);
        when(this.userAgentLiteralParser.parse(eq(userAgentString))).thenReturn(userAgentLiteral);

        UserAgentVO result = this.parserToTest.parse(userAgentString);

        assertNotNull(result);
        assertNull(result.getDevice());
        assertNull(result.getPlatform());
        assertNotNull(result.getApplication());
        assertEquals(userAgentLiteral.getApplicationName(), result.getApplication().getName());
        assertEquals(userAgentLiteral.getApplicationVersionNumber(), result.getApplication().getVersion());
    }

    @Test
    public void testWithInvalidUserAgentLiteral() {
        String userAgentString = "CustomApp/2.0 (DummyOS; DummyDevice)";

        when(this.userAgentLiteralParser.parse(eq(userAgentString))).thenReturn(null);

        try {
            this.parserToTest.parse(userAgentString);
            fail("Should throw an IllegalArgumentException!");
        } catch (IllegalArgumentException e) {
            assertEquals("User-Agent cannot be parsed: " + userAgentString, e.getMessage());
        }
    }
}
