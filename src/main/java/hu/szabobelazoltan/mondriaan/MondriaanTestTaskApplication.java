package hu.szabobelazoltan.mondriaan;

import hu.szabobelazoltan.mondriaan.dao.Dao;
import hu.szabobelazoltan.mondriaan.dao.DaoImpl;
import hu.szabobelazoltan.mondriaan.dao.SessionManager;
import hu.szabobelazoltan.mondriaan.dao.SessionManagerImpl;
import hu.szabobelazoltan.mondriaan.loader.*;
import hu.szabobelazoltan.mondriaan.model.DeviceUsage;
import hu.szabobelazoltan.mondriaan.model.Platform;
import hu.szabobelazoltan.mondriaan.model.UserAgent;
import hu.szabobelazoltan.mondriaan.parsers.ParserProvider;

import java.util.List;

public class MondriaanTestTaskApplication {

    public static void main(String[] args) {
        SessionManager sessionManager = new SessionManagerImpl();
        sessionManager.setUp();

        Dao dao = new DaoImpl(sessionManager);

        loadUserAgents(dao);

        System.out.println();
        System.out.println("************************************");


        System.out.println();
        List<DeviceUsage> mostFrequentlyUsedDevices = dao.findMostFrequentlyUsedDevices();
        for (DeviceUsage deviceUsage : mostFrequentlyUsedDevices) {
            if (deviceUsage.getDevice() != null) {
                if (deviceUsage.getDevice().getVendor() != null) {
                    System.out.println("Device vendor: " + deviceUsage.getDevice().getVendor());
                }
                System.out.println("Device model: " + deviceUsage.getDevice().getModel());
            }
            if (deviceUsage.getPlatform() != null) {
                System.out.println("Platform family: " + deviceUsage.getPlatform().getFamily());
                System.out.println(String.format("Platform version: %s", platformVersionToString(deviceUsage.getPlatform())));
            }
            System.out.println("Application name: " + deviceUsage.getApplication().getName());
            System.out.println("Application version: " + deviceUsage.getApplication().getVersion());
            System.out.println(String.format("Usage: %d", deviceUsage.getUsageCount()));
            System.out.println("----");
        }

        System.out.println();
        System.out.println();

        Platform oldestAndroidPlatform = dao.findOldestPlatformByFamily("Android");
        System.out.println(String.format("Oldest Android SDK version: %s", platformVersionToString(oldestAndroidPlatform)));

        sessionManager.tearDown();

        System.out.println("************************************");
        System.out.println();
        System.out.println();
    }

    private static void loadUserAgents(Dao dao) {
        UserAgentResourceLoader resourceLoader = new UserAgentResourceLoaderImpl(ParserProvider.USER_AGENT_PARSER);
        EntityAdapter entityAdapter = new EntityAdapterImpl();
        DataLoader dataLoader = new DataLoaderImpl(resourceLoader, entityAdapter, dao);
        dataLoader.load();
    }

    private static String platformVersionToString(Platform platform) {
        StringBuilder sb = new StringBuilder(platform.getMajorVersion());
        if (platform.getMinorVersion() != null) {
            sb.append(".");
            sb.append(platform.getMinorVersion());
        }
        if (platform.getPatchVersion() != null) {
            sb.append(".");
            sb.append(platform.getPatchVersion());
        }
        return sb.toString();
    }
}
