package hu.szabobelazoltan.mondriaan.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "USER_AGENT")
public class UserAgent {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_USER_AGENT_ID")
    @SequenceGenerator(name = "SEQ_USER_AGENT_ID", sequenceName = "SEQ_USER_AGENT_ID")
    private Long id;

    @OneToOne(fetch = FetchType.LAZY, cascade = {})
    @JoinColumn(name = "APPLICATION_ID")
    private Application application;

    @OneToOne(fetch = FetchType.LAZY, cascade = {})
    @JoinColumn(name = "PLATFORM_ID")
    private Platform platform;

    @OneToOne(fetch = FetchType.LAZY, cascade = {})
    @JoinColumn(name = "DEVICE_ID")
    private Device device;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Application getApplication() {
        return application;
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    public Platform getPlatform() {
        return platform;
    }

    public void setPlatform(Platform platform) {
        this.platform = platform;
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserAgent userAgent = (UserAgent) o;
        return Objects.equals(id, userAgent.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
