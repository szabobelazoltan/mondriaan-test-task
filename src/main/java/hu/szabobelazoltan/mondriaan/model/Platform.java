package hu.szabobelazoltan.mondriaan.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "PLATFORM")
public class Platform {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_PLATFORM_ID")
    @SequenceGenerator(name = "SEQ_PLATFORM_ID", sequenceName = "SEQ_PLATFORM_ID")
    private Long id;

    @Column(name = "FAMILY")
    private String family;

    @Column(name = "MAJOR_VERSION")
    private Integer majorVersion;

    @Column(name = "MINOR_VERSION")
    private Integer minorVersion;

    @Column(name = "PATCH_VERSION")
    private Integer patchVersion;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public Integer getMajorVersion() {
        return majorVersion;
    }

    public void setMajorVersion(Integer majorVersion) {
        this.majorVersion = majorVersion;
    }

    public Integer getMinorVersion() {
        return minorVersion;
    }

    public void setMinorVersion(Integer minorVersion) {
        this.minorVersion = minorVersion;
    }

    public Integer getPatchVersion() {
        return patchVersion;
    }

    public void setPatchVersion(Integer patchVersion) {
        this.patchVersion = patchVersion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Platform platform = (Platform) o;
        return Objects.equals(id, platform.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
