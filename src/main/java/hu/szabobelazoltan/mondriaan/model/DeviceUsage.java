package hu.szabobelazoltan.mondriaan.model;

import javax.persistence.*;

@Entity
@SqlResultSetMapping(
        name = "MostFrequentlyUsedDeviceMapping",
        entities = {@EntityResult(
                entityClass = UserAgent.class,
                fields = { @FieldResult(name = "usageCount", column = "USAGE_COUNT") }
        ),
                @EntityResult(entityClass = Application.class,
                        fields = {
                                @FieldResult(name = "id", column = "APP_ID"),
                                @FieldResult(name = "name", column = "APP_NAME"),
                                @FieldResult(name = "version", column = "APP_VERSION")
                        }
                ),
                @EntityResult(entityClass = Platform.class,
                        fields = {
                                @FieldResult(name = "id", column = "PLATFORM_ID"),
                                @FieldResult(name = "family", column = "PLATFORM_FAMILY"),
                                @FieldResult(name = "majorVersion", column = "PLATFORM_MAJOR_VERSION"),
                                @FieldResult(name = "minorVersion", column = "PLATFORM_MINOR_VERSION"),
                                @FieldResult(name = "patchVersion", column = "PLATFORM_PATCH_VERSION"),
                        }),
                @EntityResult(entityClass = Device.class,
                        fields = {
                                @FieldResult(name = "id", column = "DEVICE_ID"),
                                @FieldResult(name = "vendor", column = "DEVICE_VENDOR"),
                                @FieldResult(name = "model", column = "DEVICE_MODEL"),
                        })
        }
)
public class DeviceUsage {
    @Id
    private Long id;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "APP_ID", referencedColumnName = "ID")
    private Application application;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PLATFORM_ID", referencedColumnName = "ID")
    private Platform platform;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DEVICE_ID", referencedColumnName = "ID")
    private Device device;
    @Column(name = "USAGE_COUNT")
    private Integer usageCount;

    public Application getApplication() {
        return application;
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    public Platform getPlatform() {
        return platform;
    }

    public void setPlatform(Platform platform) {
        this.platform = platform;
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getUsageCount() {
        return usageCount;
    }

    public void setUsageCount(Integer usageCount) {
        this.usageCount = usageCount;
    }
}
