package hu.szabobelazoltan.mondriaan.dao;

import hu.szabobelazoltan.mondriaan.model.DeviceUsage;
import hu.szabobelazoltan.mondriaan.model.Platform;
import hu.szabobelazoltan.mondriaan.model.UserAgent;

import java.util.List;

public interface Dao {

    void saveAll(List entities);

    Platform findOldestPlatformByFamily(String family);

    List<DeviceUsage> findMostFrequentlyUsedDevices();
}
