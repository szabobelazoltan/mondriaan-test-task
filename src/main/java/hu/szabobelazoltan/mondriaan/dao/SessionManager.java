package hu.szabobelazoltan.mondriaan.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

public interface SessionManager {

    void setUp();

    void tearDown();

    SessionFactory getSessionFactory();

    Session getSession();

}
