package hu.szabobelazoltan.mondriaan.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class SessionManagerImpl implements SessionManager {
    private SessionFactory sessionFactory;
    private Session session;

    @Override
    public void setUp() {
        StandardServiceRegistry standardRegistry = new StandardServiceRegistryBuilder()
                .configure("hibernate.cfg.xml").build();
        Metadata metaData = new MetadataSources(standardRegistry)
                .getMetadataBuilder()
                .build();
        this.sessionFactory = metaData.getSessionFactoryBuilder().build();
        this.session = this.sessionFactory.openSession();
    }

    @Override
    public void tearDown() {
        this.session.close();
        this.sessionFactory.close();
    }

    @Override
    public SessionFactory getSessionFactory() {
        return this.sessionFactory;
    }

    @Override
    public Session getSession() {
        return this.session;
    }
}
