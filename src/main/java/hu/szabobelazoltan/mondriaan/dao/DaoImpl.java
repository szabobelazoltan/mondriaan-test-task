package hu.szabobelazoltan.mondriaan.dao;

import hu.szabobelazoltan.mondriaan.model.Application;
import hu.szabobelazoltan.mondriaan.model.Device;
import hu.szabobelazoltan.mondriaan.model.DeviceUsage;
import hu.szabobelazoltan.mondriaan.model.Platform;
import org.hibernate.Session;

import java.util.List;
import java.util.stream.Collectors;

public class DaoImpl implements Dao {
    private static final String FIND_OLDEST_PLATFORM_BY_FAMILY = "SELECT " +
            "p.ID, p.FAMILY, p.MAJOR_VERSION, p.MINOR_VERSION, p.PATCH_VERSION" +
            " FROM (SELECT p.ID FROM PUBLIC.PLATFORM p" +
            " WHERE p.FAMILY = ?" +
            " ORDER BY p.MAJOR_VERSION, p.MINOR_VERSION, p.PATCH_VERSION ASC)" +
            " s INNER JOIN PUBLIC.PLATFORM p ON p.ID = s.ID LIMIT 1;";

    private static final String FIND_MOST_FREQUENTLY_USED_DEVICES = "SELECT " +
            "  d.ID AS DEVICE_ID," +
            "  d.VENDOR AS DEVICE_VENDOR," +
            "  d.MODEL AS DEVICE_MODEL," +
            "  o.DEVICE_COUNT AS USAGE_COUNT," +
            "  a.ID AS APP_ID, " +
            "  a.NAME AS APP_NAME," +
            "  a.VERSION AS APP_VERSION," +
            "  p.ID AS PLATFORM_ID," +
            "  p.FAMILY AS PLATFORM_FAMILY," +
            "  p.MAJOR_VERSION AS PLATFORM_MAJOR_VERSION," +
            "  p.MINOR_VERSION AS PLATFORM_MINOR_VERSION," +
            "  p.PATCH_VERSION AS PLATFORM_PATCH_VERSION" +
            " FROM " +
            "(SELECT ua.DEVICE_ID, ua.APPLICATION_ID, ua.PLATFORM_ID, count(ua.DEVICE_ID) AS DEVICE_COUNT FROM PUBLIC.USER_AGENT ua  " +
            "GROUP BY ua.DEVICE_ID, ua.APPLICATION_ID, ua.PLATFORM_ID) o " +
            "INNER JOIN  " +
            "(SELECT max(a.DEVICE_COUNT) AS MAX_DEVICE_COUNT, a.APPLICATION_ID, a.PLATFORM_ID FROM  " +
            "(SELECT ua.DEVICE_ID, ua.APPLICATION_ID, ua.PLATFORM_ID, count(ua.DEVICE_ID) AS DEVICE_COUNT FROM PUBLIC.USER_AGENT ua GROUP BY ua.DEVICE_ID, ua.APPLICATION_ID, ua.PLATFORM_ID) a " +
            "GROUP BY a.APPLICATION_ID, a.PLATFORM_ID) s " +
            "ON o.DEVICE_COUNT = s.MAX_DEVICE_COUNT AND o.APPLICATION_ID = s.APPLICATION_ID AND o.PLATFORM_ID = s.PLATFORM_ID " +
            "INNER JOIN APPLICATION a ON o.APPLICATION_ID = a.ID " +
            "INNER JOIN PLATFORM p ON o.PLATFORM_ID = p.ID " +
            "INNER JOIN DEVICE d ON o.DEVICE_ID = d.ID " +
            "ORDER BY o.DEVICE_COUNT DESC";

    private final SessionManager sessionManager;

    public DaoImpl(SessionManager sessionManager) {
        this.sessionManager = sessionManager;
    }

    @Override
    public void saveAll(List entities) {
        Session session = sessionManager.getSession();
        session.beginTransaction();
        entities.forEach(session::save);
        session.getTransaction().commit();
    }

    @Override
    public Platform findOldestPlatformByFamily(String family) {
        return sessionManager.getSession().createNativeQuery(FIND_OLDEST_PLATFORM_BY_FAMILY, Platform.class)
                .setParameter(1, family)
                .getSingleResult();
    }

    @Override
    public List<DeviceUsage> findMostFrequentlyUsedDevices() {
        List<DeviceUsage> result = sessionManager.getSession().createNativeQuery(FIND_MOST_FREQUENTLY_USED_DEVICES, DeviceUsage.class)
                .getResultList();
        return result;
    }
}
