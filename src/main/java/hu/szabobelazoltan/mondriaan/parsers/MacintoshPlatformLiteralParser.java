package hu.szabobelazoltan.mondriaan.parsers;

import hu.szabobelazoltan.mondriaan.vos.PlatformLiteralVO;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MacintoshPlatformLiteralParser extends AbstractUserAgentLiteralParser<PlatformLiteralVO> {
    private static final Pattern MACINTOSH_PLATFORM_REGEX_PATTERN = Pattern.compile("(Macintosh); Intel (Mac OS X) ([\\d_]+)");

    public MacintoshPlatformLiteralParser(AbstractUserAgentLiteralParser<PlatformLiteralVO> nextParser) {
        super(nextParser);
    }

    public MacintoshPlatformLiteralParser() {
    }

    @Override
    protected PlatformLiteralVO tryParse(String value) {
        Matcher platformMatcher = MACINTOSH_PLATFORM_REGEX_PATTERN.matcher(value);
        if (!platformMatcher.find()) {
            return null;
        }
        String model = platformMatcher.group(1);
        String family = platformMatcher.group(2);
        String version = platformMatcher.group(3);
        return new PlatformLiteralVO(family, version, null, model);
    }
}
