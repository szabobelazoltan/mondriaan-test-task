package hu.szabobelazoltan.mondriaan.parsers;

import hu.szabobelazoltan.mondriaan.vos.PlatformLiteralVO;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ApplePlatformLiteralParser extends AbstractUserAgentLiteralParser<PlatformLiteralVO> {
    private static final Pattern APPLE_PLATFORM_REGEX_PATTERN = Pattern.compile("(iOS)/([\\d\\.]+) \\((iPhone\\d+,\\d+|iPad\\d+,\\d+)\\)");

    public ApplePlatformLiteralParser(AbstractUserAgentLiteralParser<PlatformLiteralVO> nextParser) {
        super(nextParser);
    }

    public ApplePlatformLiteralParser() {
    }

    @Override
    protected PlatformLiteralVO tryParse(String value) {
        Matcher platformMatcher = APPLE_PLATFORM_REGEX_PATTERN.matcher(value);
        if (!platformMatcher.find()) {
            return null;
        }
        String family = platformMatcher.group(1);
        String version = platformMatcher.group(2);
        String model = platformMatcher.group(3);
        return new PlatformLiteralVO(family, version, null, model);
    }
}
