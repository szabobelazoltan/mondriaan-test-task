package hu.szabobelazoltan.mondriaan.parsers;

public abstract class AbstractUserAgentLiteralParser<TResult> implements UserAgentParser<TResult> {
    private final AbstractUserAgentLiteralParser<TResult> nextParser;

    public AbstractUserAgentLiteralParser(AbstractUserAgentLiteralParser<TResult> nextParser) {
        this.nextParser = nextParser;
    }

    public AbstractUserAgentLiteralParser() {
        this(null);
    }

    @Override
    public TResult parse(String value) {
        TResult result = tryParse(value);
        if (result != null) {
            return result;
        } else if (this.nextParser != null) {
            return this.nextParser.parse(value);
        } else {
            return null;
        }
    }

    protected abstract TResult tryParse(String value);
}
