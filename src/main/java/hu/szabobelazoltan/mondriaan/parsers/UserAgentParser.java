package hu.szabobelazoltan.mondriaan.parsers;

public interface UserAgentParser<TResult> {

    TResult parse(String value);
}
