package hu.szabobelazoltan.mondriaan.parsers;

import hu.szabobelazoltan.mondriaan.vos.PlatformLiteralVO;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AndroidPlatformLiteralParser extends AbstractUserAgentLiteralParser<PlatformLiteralVO> {
    private static final Pattern ANDROID_PLATFORM_LITERAL_REGEX_PATTERN = Pattern.compile("Linux;( U;)? (Android) ([\\d\\.]+); (\\w+ )?([\\w\\d-]+)");

    public AndroidPlatformLiteralParser(AbstractUserAgentLiteralParser<PlatformLiteralVO> nextParser) {
        super(nextParser);
    }

    public AndroidPlatformLiteralParser() {
    }

    @Override
    protected PlatformLiteralVO tryParse(String value) {
        String platformString = value.replaceAll("\\sBuild/.+", "");
        Matcher platformLiteralMatcher = ANDROID_PLATFORM_LITERAL_REGEX_PATTERN.matcher(platformString);
        if (!platformLiteralMatcher.find()) {
            return null;
        }

        String family = platformLiteralMatcher.group(2);
        String version = platformLiteralMatcher.group(3);
        String vendor = platformLiteralMatcher.group(4) != null ? platformLiteralMatcher.group(4).trim() : null;
        String model = platformLiteralMatcher.group(5);
        return new PlatformLiteralVO(family, version, vendor, model);
    }
}
