package hu.szabobelazoltan.mondriaan.parsers;

import hu.szabobelazoltan.mondriaan.vos.UserAgentLiteralVO;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TvGuideUserAgentLiteralParser extends AbstractUserAgentLiteralParser<UserAgentLiteralVO> {
    private static final Pattern TV_GUIDE_UA_REGEX_PATTERN = Pattern.compile("(TV-Guide)/([\\d\\.]+) CFNetwork/[\\d\\.]+ Darwin/[\\d\\.]+");

    @Override
    protected UserAgentLiteralVO tryParse(String value) {
        Matcher uaMatcher = TV_GUIDE_UA_REGEX_PATTERN.matcher(value);
        if (!uaMatcher.find()) {
            return null;
        }
        String appName = uaMatcher.group(1);
        String appVersion = uaMatcher.group(2);
        return new UserAgentLiteralVO(appName, appVersion, null);
    }
}
