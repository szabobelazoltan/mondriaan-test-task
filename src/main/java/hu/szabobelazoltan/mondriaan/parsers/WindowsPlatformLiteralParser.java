package hu.szabobelazoltan.mondriaan.parsers;

import hu.szabobelazoltan.mondriaan.vos.PlatformLiteralVO;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WindowsPlatformLiteralParser extends AbstractUserAgentLiteralParser<PlatformLiteralVO> {
    private static final Pattern WINDOWS_PLATFORM_REGEX_PATTERN = Pattern.compile("(Windows NT) ([\\d\\.]+); (Win\\d+); [\\w\\d-]+");

    public WindowsPlatformLiteralParser(AbstractUserAgentLiteralParser<PlatformLiteralVO> nextParser) {
        super(nextParser);
    }

    public WindowsPlatformLiteralParser() {
    }

    @Override
    protected PlatformLiteralVO tryParse(String value) {
        Matcher platformMatcher = WINDOWS_PLATFORM_REGEX_PATTERN.matcher(value);
        if (!platformMatcher.find()) {
            return null;
        }
        String family = platformMatcher.group(1);
        String version = platformMatcher.group(2);
        return new PlatformLiteralVO(family, version, null, null);
    }
}
