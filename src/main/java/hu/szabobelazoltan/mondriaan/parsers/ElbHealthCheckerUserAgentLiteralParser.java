package hu.szabobelazoltan.mondriaan.parsers;

import hu.szabobelazoltan.mondriaan.vos.UserAgentLiteralVO;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ElbHealthCheckerUserAgentLiteralParser extends AbstractUserAgentLiteralParser<UserAgentLiteralVO> {
    private static final Pattern ELB_HEALTHCHECKER_UA_REGEX_PATTERN = Pattern.compile("(ELB-HealthChecker)/([\\d\\.]+)");

    public ElbHealthCheckerUserAgentLiteralParser(AbstractUserAgentLiteralParser<UserAgentLiteralVO> nextParser) {
        super(nextParser);
    }

    public ElbHealthCheckerUserAgentLiteralParser() {
    }

    @Override
    protected UserAgentLiteralVO tryParse(String value) {
        Matcher uaMatcher = ELB_HEALTHCHECKER_UA_REGEX_PATTERN.matcher(value);
        if (!uaMatcher.find()) {
            return null;
        }

        String appName = uaMatcher.group(1);
        String appVersion = uaMatcher.group(2);
        return new UserAgentLiteralVO(appName, appVersion, null);
    }
}
