package hu.szabobelazoltan.mondriaan.parsers;

import hu.szabobelazoltan.mondriaan.vos.UserAgentLiteralVO;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ChromeUserAgentParser extends AbstractUserAgentLiteralParser<UserAgentLiteralVO> {
    private static final Pattern CHROME_UA_REGEX_PATTERN = Pattern.compile("Mozilla/5.0 \\((.+)\\) AppleWebKit/[\\d\\.]+ \\(KHTML, like Gecko\\)( \\w+/[\\d\\.]+)? (Chrome)/([\\d\\.]+)( Mobile)? Safari/[\\d\\.]+");

    public ChromeUserAgentParser(AbstractUserAgentLiteralParser<UserAgentLiteralVO> nextParser) {
        super(nextParser);
    }

    public ChromeUserAgentParser() {
    }

    @Override
    protected UserAgentLiteralVO tryParse(String value) {
        Matcher uaMatcher = CHROME_UA_REGEX_PATTERN.matcher(value);
        if (!uaMatcher.find()) {
            return null;
        }
        String platformLiteral = uaMatcher.group(1);
        String appName = uaMatcher.group(3);
        String appVersion = uaMatcher.group(4);
        return new UserAgentLiteralVO(appName, appVersion, platformLiteral);
    }
}
