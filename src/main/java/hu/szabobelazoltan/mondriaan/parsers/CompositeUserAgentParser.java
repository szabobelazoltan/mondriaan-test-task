package hu.szabobelazoltan.mondriaan.parsers;

import hu.szabobelazoltan.mondriaan.vos.*;

public class CompositeUserAgentParser implements UserAgentParser<UserAgentVO> {
    private final UserAgentParser<UserAgentLiteralVO> userAgentParser;
    private final UserAgentParser<PlatformLiteralVO> platformParser;

    public CompositeUserAgentParser(UserAgentParser<UserAgentLiteralVO> userAgentParser, UserAgentParser<PlatformLiteralVO> platformParser) {
        this.userAgentParser = userAgentParser;
        this.platformParser = platformParser;
    }

    @Override
    public UserAgentVO parse(String value) {
        String userAgent = value.replaceFirst("User-Agent: ", "");
        UserAgentLiteralVO userAgentLiteralVO = userAgentParser.parse(userAgent);
        if (userAgentLiteralVO == null) {
            throw new IllegalArgumentException(String.format("User-Agent cannot be parsed: %s", value));
        }
        PlatformLiteralVO platformLiteralVO = parsePlatformLiteral(userAgentLiteralVO.getPlatformInfo());
        return createUserAgentFromLiteralVOs(userAgentLiteralVO, platformLiteralVO);
    }

    private PlatformLiteralVO parsePlatformLiteral(String platformInfo) {
        if (platformInfo != null) {
            return platformParser.parse(platformInfo);
        } else {
            return null;
        }
    }

    private UserAgentVO createUserAgentFromLiteralVOs(UserAgentLiteralVO userAgentLiteralVO, PlatformLiteralVO platformLiteralVO) {
        DeviceVO deviceVO = null;
        PlatformVO platformVO = null;
        if (platformLiteralVO != null
                && (platformLiteralVO.getDeviceModel() != null || platformLiteralVO.getDeviceVendor() != null)) {
            deviceVO = new DeviceVO(platformLiteralVO.getDeviceVendor(), platformLiteralVO.getDeviceModel());
        }
        if (platformLiteralVO != null
                && (platformLiteralVO.getPlatformFamily() != null || platformLiteralVO.getPlatformVersionNumber() != null)) {
            platformVO = new PlatformVO(platformLiteralVO.getPlatformFamily(), platformLiteralVO.getPlatformVersionNumber());
        }
        ApplicationVO applicationVO = new ApplicationVO(userAgentLiteralVO.getApplicationName(), userAgentLiteralVO.getApplicationVersionNumber());
        return new UserAgentVO(deviceVO, platformVO, applicationVO);
    }
}
