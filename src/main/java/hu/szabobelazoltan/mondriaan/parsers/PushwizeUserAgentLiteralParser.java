package hu.szabobelazoltan.mondriaan.parsers;

import hu.szabobelazoltan.mondriaan.vos.UserAgentLiteralVO;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PushwizeUserAgentLiteralParser extends AbstractUserAgentLiteralParser<UserAgentLiteralVO> {
    private static final List<Pattern> PUSHWIZE_UA_REGEX_PATTERNS = Arrays.asList(
            Pattern.compile("Pushwize/?[\\d\\.]* ([\\w\\d\\+-]+)/([\\d\\.]+) \\(com\\.mondriaan[\\.\\w]+/\\d+\\) Dalvik/[\\d\\.]+ \\((.+)\\)"),
            Pattern.compile("Pushwize/?[\\d\\.]* ([\\w\\d\\+-]+)/([\\d\\.]+) \\(com\\.mondriaan[\\w\\.]*\\.\\w+/\\d+\\) (iOS/[\\d\\.]+ \\(.+\\))"),
            Pattern.compile("(Dalvik)/([\\d\\.]+) \\((.+)\\)"),
            Pattern.compile("Pushwize/?[\\d\\.]* ([\\w\\d\\+-]+)/([\\d\\.]+) \\(com\\.mondriaan[\\.\\w]+/\\d+\\) Mozilla/[\\d\\.]+ \\((.+)\\) AppleWebKit/[\\d\\.]+ \\(KHTML, like Gecko\\) Version/[\\d\\.]+ Mobile Safari/[\\d\\.]+")
    );

    public PushwizeUserAgentLiteralParser(AbstractUserAgentLiteralParser<UserAgentLiteralVO> nextParser) {
        super(nextParser);
    }

    public PushwizeUserAgentLiteralParser() {
    }

    @Override
    protected UserAgentLiteralVO tryParse(String value) {
        for (Pattern uaPattern : PUSHWIZE_UA_REGEX_PATTERNS) {
            Matcher uaMatcher = uaPattern.matcher(value);
            if (uaMatcher.find()) {
                String appName = uaMatcher.group(1);
                String appVersion = uaMatcher.group(2);
                String platformInfo = uaMatcher.group(3);
                return new UserAgentLiteralVO(appName, appVersion, platformInfo);
            }
        }
        return null;
    }
}
