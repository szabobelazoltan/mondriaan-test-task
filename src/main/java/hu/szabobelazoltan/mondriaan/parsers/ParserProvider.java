package hu.szabobelazoltan.mondriaan.parsers;

import hu.szabobelazoltan.mondriaan.vos.PlatformLiteralVO;
import hu.szabobelazoltan.mondriaan.vos.UserAgentLiteralVO;
import hu.szabobelazoltan.mondriaan.vos.UserAgentVO;

public class ParserProvider {
    public static UserAgentParser<UserAgentLiteralVO> USER_AGENT_LITERAL_PARSER = new ChromeUserAgentParser(new PushwizeUserAgentLiteralParser(new ElbHealthCheckerUserAgentLiteralParser(new AmazonUserAgentLiteralParser(new TvGuideUserAgentLiteralParser()))));

    public static UserAgentParser<PlatformLiteralVO> PLATFORM_LITERAL_PARSER = new AndroidPlatformLiteralParser(new WindowsPlatformLiteralParser(new ApplePlatformLiteralParser(new MacintoshPlatformLiteralParser())));

    public static UserAgentParser<UserAgentVO> USER_AGENT_PARSER = new CompositeUserAgentParser(USER_AGENT_LITERAL_PARSER, PLATFORM_LITERAL_PARSER);
}
