package hu.szabobelazoltan.mondriaan.parsers;

import hu.szabobelazoltan.mondriaan.vos.UserAgentLiteralVO;

public class AmazonUserAgentLiteralParser extends AbstractUserAgentLiteralParser<UserAgentLiteralVO> {
    public AmazonUserAgentLiteralParser(AbstractUserAgentLiteralParser<UserAgentLiteralVO> nextParser) {
        super(nextParser);
    }

    public AmazonUserAgentLiteralParser() {
    }

    @Override
    protected UserAgentLiteralVO tryParse(String value) {
        if ("Amazon CloudFront".equals(value)) {
            UserAgentLiteralVO vo = new UserAgentLiteralVO("Amazon CloudFront", null, null);
            return vo;
        } else {
            return null;
        }
    }
}
