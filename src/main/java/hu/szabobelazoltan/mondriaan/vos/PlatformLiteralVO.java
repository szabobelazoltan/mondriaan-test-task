package hu.szabobelazoltan.mondriaan.vos;

public class PlatformLiteralVO {
    private final String platformFamily;
    private final String platformVersionNumber;
    private final String deviceVendor;
    private final String deviceModel;

    public PlatformLiteralVO(String platformFamily, String platformVersionNumber, String deviceVendor, String deviceModel) {
        this.platformFamily = platformFamily;
        this.platformVersionNumber = platformVersionNumber;
        this.deviceVendor = deviceVendor;
        this.deviceModel = deviceModel;
    }

    public String getPlatformFamily() {
        return platformFamily;
    }

    public String getPlatformVersionNumber() {
        return platformVersionNumber;
    }

    public String getDeviceVendor() {
        return deviceVendor;
    }

    public String getDeviceModel() {
        return deviceModel;
    }
}
