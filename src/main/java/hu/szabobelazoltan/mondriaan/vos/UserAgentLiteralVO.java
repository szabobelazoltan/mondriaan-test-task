package hu.szabobelazoltan.mondriaan.vos;

public class UserAgentLiteralVO {
    private final String applicationName;
    private final String applicationVersionNumber;
    private final String platformInfo;

    public UserAgentLiteralVO(String applicationName, String applicationVersionNumber, String platformInfo) {
        this.applicationName = applicationName;
        this.applicationVersionNumber = applicationVersionNumber;
        this.platformInfo = platformInfo;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public String getApplicationVersionNumber() {
        return applicationVersionNumber;
    }

    public String getPlatformInfo() {
        return platformInfo;
    }
}
