package hu.szabobelazoltan.mondriaan.vos;

public class UserAgentVO {
    private final DeviceVO device;
    private final PlatformVO platform;
    private final ApplicationVO application;

    public UserAgentVO(DeviceVO device, PlatformVO platform, ApplicationVO application) {
        this.device = device;
        this.platform = platform;
        this.application = application;
    }

    public DeviceVO getDevice() {
        return device;
    }

    public PlatformVO getPlatform() {
        return platform;
    }

    public ApplicationVO getApplication() {
        return application;
    }
}
