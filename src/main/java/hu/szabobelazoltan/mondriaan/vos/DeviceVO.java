package hu.szabobelazoltan.mondriaan.vos;

import java.util.Objects;

public class DeviceVO {
    private final String vendor;
    private final String model;

    public DeviceVO(String vendor, String model) {
        this.vendor = vendor;
        this.model = model;
    }

    public String getVendor() {
        return vendor;
    }

    public String getModel() {
        return model;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DeviceVO deviceVO = (DeviceVO) o;
        return Objects.equals(vendor, deviceVO.vendor) && Objects.equals(model, deviceVO.model);
    }

    @Override
    public int hashCode() {
        return Objects.hash(vendor, model);
    }
}
