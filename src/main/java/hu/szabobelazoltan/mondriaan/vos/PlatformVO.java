package hu.szabobelazoltan.mondriaan.vos;

import java.util.Objects;

public class PlatformVO {
    private final String family;
    private final String version;

    public PlatformVO(String family, String version) {
        this.family = family;
        this.version = version;
    }

    public String getFamily() {
        return family;
    }

    public String getVersion() {
        return version;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PlatformVO that = (PlatformVO) o;
        return Objects.equals(family, that.family) && Objects.equals(version, that.version);
    }

    @Override
    public int hashCode() {
        return Objects.hash(family, version);
    }
}
