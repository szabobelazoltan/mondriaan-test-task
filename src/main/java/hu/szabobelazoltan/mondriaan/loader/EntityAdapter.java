package hu.szabobelazoltan.mondriaan.loader;

import hu.szabobelazoltan.mondriaan.model.Application;
import hu.szabobelazoltan.mondriaan.model.Device;
import hu.szabobelazoltan.mondriaan.model.Platform;
import hu.szabobelazoltan.mondriaan.model.UserAgent;
import hu.szabobelazoltan.mondriaan.vos.UserAgentVO;

import java.util.List;

public interface EntityAdapter {

    void processVOs(List<UserAgentVO> vos);

    List<Device> getDeviceEntities();

    List<Platform> getPlatformEntities();

    List<Application> getApplicationEntities();

    List<UserAgent> getUserAgentEntities();
}
