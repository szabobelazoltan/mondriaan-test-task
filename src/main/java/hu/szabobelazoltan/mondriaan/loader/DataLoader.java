package hu.szabobelazoltan.mondriaan.loader;

public interface DataLoader {

    void load();
}
