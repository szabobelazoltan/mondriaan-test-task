package hu.szabobelazoltan.mondriaan.loader;

import hu.szabobelazoltan.mondriaan.model.Application;
import hu.szabobelazoltan.mondriaan.model.Device;
import hu.szabobelazoltan.mondriaan.model.Platform;
import hu.szabobelazoltan.mondriaan.model.UserAgent;
import hu.szabobelazoltan.mondriaan.vos.ApplicationVO;
import hu.szabobelazoltan.mondriaan.vos.DeviceVO;
import hu.szabobelazoltan.mondriaan.vos.PlatformVO;
import hu.szabobelazoltan.mondriaan.vos.UserAgentVO;

import java.util.*;
import java.util.function.Function;

public class EntityAdapterImpl implements EntityAdapter {
    private final Map<DeviceVO, Device> deviceMap = new HashMap<>();
    private final Map<PlatformVO, Platform> platformMap = new HashMap<>();
    private final Map<ApplicationVO, Application> appMap = new HashMap<>();
    private final List<UserAgent> userAgents = new ArrayList<>();

    @Override
    public void processVOs(List<UserAgentVO> vos) {
        for (UserAgentVO vo : vos) {
            UserAgent userAgent = createUserAgentFromVo(vo);
            this.userAgents.add(userAgent);
        }
    }

    @Override
    public List<Device> getDeviceEntities() {
        return new ArrayList<>(this.deviceMap.values());
    }

    @Override
    public List<Platform> getPlatformEntities() {
        return new ArrayList<>(this.platformMap.values());
    }

    @Override
    public List<Application> getApplicationEntities() {
        return new ArrayList<>(this.appMap.values());
    }

    @Override
    public List<UserAgent> getUserAgentEntities() {
        return this.userAgents;
    }

    private UserAgent createUserAgentFromVo(UserAgentVO vo) {
        Device device = getConnectedEntity(vo.getDevice(), deviceMap, this::mapDevice);
        Platform platform = getConnectedEntity(vo.getPlatform(), platformMap, this::mapPlatform);
        Application application = getConnectedEntity(vo.getApplication(), appMap, this::mapApp);

        UserAgent userAgent = new UserAgent();
        userAgent.setDevice(device);
        userAgent.setPlatform(platform);
        userAgent.setApplication(application);
        return userAgent;
    }

    private <TVo, TEntity> TEntity getConnectedEntity(TVo vo, Map<TVo, TEntity> map, Function<TVo, TEntity> mapperFunction) {
        if (vo == null) {
            return null;
        } else if (map.containsKey(vo)) {
            return map.get(vo);
        } else {
            TEntity entity = mapperFunction.apply(vo);
            map.put(vo, entity);
            return entity;
        }
    }

    private Device mapDevice(DeviceVO vo) {
        Device entity = new Device();
        entity.setVendor(vo.getVendor());
        entity.setModel(vo.getModel());
        return entity;
    }

    private Platform mapPlatform(PlatformVO vo) {
        Platform entity = new Platform();
        entity.setFamily(vo.getFamily());
        String[] versionNumbers = vo.getVersion().split("\\.|_");
        entity.setMajorVersion(Integer.parseInt(versionNumbers[0]));
        if (versionNumbers.length > 1) {
            entity.setMinorVersion(Integer.parseInt(versionNumbers[1]));
        }
        if (versionNumbers.length > 2) {
            entity.setPatchVersion(Integer.parseInt(versionNumbers[2]));
        }
        return entity;
    }

    private Application mapApp(ApplicationVO vo) {
        Application entity = new Application();
        entity.setName(vo.getName());
        entity.setVersion(vo.getVersion());
        return entity;
    }
}
