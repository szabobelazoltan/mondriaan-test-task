package hu.szabobelazoltan.mondriaan.loader;

import hu.szabobelazoltan.mondriaan.vos.UserAgentVO;

import java.util.List;

public interface UserAgentResourceLoader {

    void load();

    List<UserAgentVO> getUserAgents();
}
