package hu.szabobelazoltan.mondriaan.loader;

import hu.szabobelazoltan.mondriaan.dao.Dao;
import hu.szabobelazoltan.mondriaan.vos.UserAgentVO;

import java.util.List;

public class DataLoaderImpl implements DataLoader {
    private final UserAgentResourceLoader resourceLoader;
    private final EntityAdapter entityAdapter;
    private final Dao dao;

    public DataLoaderImpl(UserAgentResourceLoader resourceLoader, EntityAdapter entityAdapter, Dao dao) {
        this.resourceLoader = resourceLoader;
        this.entityAdapter = entityAdapter;
        this.dao = dao;
    }

    @Override
    public void load() {
        this.resourceLoader.load();
        List<UserAgentVO> userAgentVOs = this.resourceLoader.getUserAgents();
        this.entityAdapter.processVOs(userAgentVOs);
        dao.saveAll(this.entityAdapter.getDeviceEntities());
        dao.saveAll(this.entityAdapter.getPlatformEntities());
        dao.saveAll(this.entityAdapter.getApplicationEntities());
        dao.saveAll(this.entityAdapter.getUserAgentEntities());
    }
}
