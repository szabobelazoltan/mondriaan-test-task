package hu.szabobelazoltan.mondriaan.loader;

import hu.szabobelazoltan.mondriaan.parsers.UserAgentParser;
import hu.szabobelazoltan.mondriaan.vos.UserAgentVO;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class UserAgentResourceLoaderImpl implements UserAgentResourceLoader {
    private static final String RESOURCE_FILENAME = "user-agent.txt";

    private final UserAgentParser<UserAgentVO> userAgentParser;
    private final List<UserAgentVO> userAgents = new ArrayList<>();

    public UserAgentResourceLoaderImpl(UserAgentParser<UserAgentVO> userAgentParser) {
        this.userAgentParser = userAgentParser;
    }

    @Override
    public void load() {
        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(RESOURCE_FILENAME)) {
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                UserAgentVO userAgentVO = this.userAgentParser.parse(line);
                this.userAgents.add(userAgentVO);
            }
        } catch (IOException e) {

        }
    }

    @Override
    public List<UserAgentVO> getUserAgents() {
        return this.userAgents;
    }
}
